#include "framework.h"

int		g_nZoomReset = 0 ;

BOOL		g_bIsFileTreeBarHoverResizing = FALSE ;
BOOL		g_bIsFileTreeBarResizing = FALSE ;
BOOL		g_bIsTabPageMoving = FALSE ;
int		g_nTabPageMoveFrom = 0 ;
POINT		g_rectLMouseDown = { 0 } ;
BOOL		g_bIsFunctionListHoverResizing = FALSE ;
BOOL		g_bIsFunctionListResizing = FALSE ;
BOOL		g_bIsTreeViewHoverResizing = FALSE ;
BOOL		g_bIsTreeViewResizing = FALSE ;
BOOL		g_bIsSqlQueryResultEditHoverResizing = FALSE ;
BOOL		g_bIsSqlQueryResultEditResizing = FALSE ;
BOOL		g_bIsSqlQueryResultListViewHoverResizing = FALSE ;
BOOL		g_bIsSqlQueryResultListViewResizing = FALSE ;

int BeforeWndProc( MSG *p_msg )
{
	NMHDR		*lpnmhdr = NULL;

	int		nret = 0 ;

	if( p_msg->message == WM_SYSKEYDOWN && 49 <= p_msg->wParam && p_msg->wParam <= 57 && (p_msg->lParam&(1<<29)) )
	{
		struct TabPage	*pnodeTabPage = SelectTabPageByIndex( (unsigned int)(p_msg->wParam) - 49 ) ;
		if( pnodeTabPage )
			return 1;
	}

	if( p_msg->message == WM_MOUSEMOVE )
	{
		WPARAM wParam ;
		LPARAM lParam ;
		POINT pt ;
		wParam = p_msg->wParam ;
		lParam = p_msg->lParam ;
		pt.x = LOWORD(lParam) ;
		pt.y = HIWORD(lParam) ;
		if( p_msg->hwnd != g_hwndMainWindow )
		{
			::ClientToScreen( p_msg->hwnd , & pt );
			::ScreenToClient( g_hwndMainWindow , & pt );
			lParam = MAKELONG( pt.x , pt.y ) ;
		}
		wParam = MAKELONG( IDM_MOUSEMOVE , 0 ) ;
		::PostMessage( g_hwndMainWindow , WM_COMMAND , wParam , lParam);
	}

	if( p_msg->message == WM_LBUTTONUP )
	{
		if( p_msg->hwnd != g_hwndMainWindow )
		{
			::PostMessage( g_hwndMainWindow , WM_LBUTTONUP , p_msg->wParam , p_msg->lParam);
		}
	}

	if( p_msg->hwnd == g_hwndFileTree )
	{
		if( p_msg->message == WM_RBUTTONDOWN )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			TVHITTESTINFO tvhti ;
			memset( & tvhti , 0x00 , sizeof(tvhti) );
			memcpy( & (tvhti.pt) , & pt , sizeof(POINT) );
			TreeView_HitTest( g_hwndFileTree , & tvhti );
			struct TreeViewData *tvd = GetTreeViewDataFromHTREEITEM( tvhti.hItem ) ;
			if( tvd )
			{
				::ClientToScreen( p_msg->hwnd , & pt );
				if( tvd->nImageIndex == nFileTreeImageDrive || tvd->nImageIndex == nFileTreeImageOpenFold || tvd->nImageIndex == nFileTreeImageClosedFold )
					::TrackPopupMenu( g_hFileTreeDirectoryPopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
				else
					::TrackPopupMenu( g_hFileTreeFilePopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
			}
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_REFRESH_FILETREE )
		{
			RefreshFileTree();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_CREATE_SUB_DIRECTORY )
		{
			FileTreeCreateSubDirectory();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RENAME_DIRECTORY )
		{
			FileTreeRenameDirectory();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_DELETE_DIRECTORY )
		{
			FileTreeDeleteDirectory();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_CREATE_FILE )
		{
			FileTreeCreateFile();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_COPY_FILE )
		{
			FileTreeCopyFile();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RENAME_FILE )
		{
			FileTreeRenameFile();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_DELETE_FILE )
		{
			FileTreeDeleteFile();
			return 1;
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_hwndTabPages )
	{
		if( p_msg->message == WM_LBUTTONDOWN )
		{
			if( g_bIsTabPageMoving == FALSE )
			{
				int x = LOWORD(p_msg->lParam) ;
				int y = HIWORD(p_msg->lParam) ;

				int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				RECT rectTabPage ;
				g_nTabPageMoveFrom = -1 ;
				for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
				{
					TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
					if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
					{
						g_nTabPageMoveFrom = nTabPageIndex ;
						break;
					}
				}
				if( g_nTabPageMoveFrom >= 0 )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEALL) );
					g_bIsTabPageMoving = TRUE ;
					g_rectLMouseDown.x = x ;
					g_rectLMouseDown.y = y ;
				}
			}
		}
		else if( p_msg->message == WM_LBUTTONUP )
		{
			if( g_bIsTabPageMoving == TRUE )
			{
				int x = LOWORD(p_msg->lParam) ;
				int y = HIWORD(p_msg->lParam) ;

				if( abs(x-g_rectLMouseDown.x) > MOUSE_MOVE_THRESHOLD_FROM_DRAG_OR_CLICK || abs(y-g_rectLMouseDown.y) > MOUSE_MOVE_THRESHOLD_FROM_DRAG_OR_CLICK )
				{
					int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
					RECT rectTabPage ;
					int nTabPageMoveTo = -1 ;
					for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
					{
						TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
						if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
						{
							nTabPageMoveTo = nTabPageIndex ;
							break;
						}
					}
					if( nTabPageMoveTo >= 0 )
					{
						TabCtrl_DeleteItem( g_hwndTabPages , g_nTabPageMoveFrom );

						TCITEM		tci ;

						memset( & tci , 0x00 , sizeof(TCITEM) );
						tci.mask = TCIF_TEXT|TCIF_PARAM ;
						tci.pszText = g_pnodeCurrentTabPage->acFilename ;
						tci.lParam = (LPARAM)g_pnodeCurrentTabPage ;
						nret = TabCtrl_InsertItem( g_hwndTabPages , nTabPageMoveTo , & tci ) ;
						if( nret == -1 )
						{
							::MessageBox(NULL, TEXT("TabCtrl_InsertItem失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
						}
						else
						{
							SelectTabPageByIndex( nTabPageMoveTo );
						}
					}
				}

				::SetCursor( LoadCursor(NULL,IDC_ARROW) );
				g_bIsTabPageMoving = FALSE ;
			}
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndScintilla )
	{
		if( p_msg->message == WM_KEYDOWN )
		{
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , p_msg->wParam , (::GetKeyState(VK_CONTROL)&0x8000)?VK_CONTROL:0 );
		}

		if( p_msg->message == WM_KEYDOWN )
		{
			if( g_pnodeCurrentTabPage->bHexEditMode == TRUE )
			{
				nret = AdujstKeyOnHexEditMode( g_pnodeCurrentTabPage , p_msg ) ;
				if( nret == 1 )
					return 1;
			}
		}

		if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hEditorPopupMenu , 0 , pt.x , pt.y , 0 , g_hwndMainWindow , NULL );
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndSymbolList )
	{
		if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hSymbolListPopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RELOAD_SYMBOLLIST )
		{
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( g_pnodeCurrentTabPage );
			return 1;
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndSymbolTree )
	{
		if( p_msg->message == WM_LBUTTONDBLCLK )
		{
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree( g_pnodeCurrentTabPage );
			return 1;
		}
		else if( p_msg->message == WM_RBUTTONDOWN )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hSymbolTreePopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RELOAD_SYMBOLTREE )
		{
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree( g_pnodeCurrentTabPage );
			return 1;
		}
	}

	return 0;
}

int BeforeDispatchMessage( MSG *p_msg )
{
	int		nret = 0 ;

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndScintilla )
	{
		if( p_msg->message == WM_CHAR )
		{
			if( g_pnodeCurrentTabPage->bHexEditMode == TRUE )
			{
				nret = AdujstCharOnHexEditMode( g_pnodeCurrentTabPage , p_msg ) ;
				if( nret == 1 )
					return 1;
			}
		}
	}

	return 0;
}

int AfterWndProc( MSG *p_msg )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	struct TabPage	*p = NULL ;
	TCITEM		tci ;

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_hwndTabPages )
	{
		if( p_msg->message == WM_RBUTTONDOWN )
		{
			int x = LOWORD(p_msg->lParam) ;
			int y = HIWORD(p_msg->lParam) ;
			int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
			RECT rectTabPage , rectFileTreeBar ;
			for( nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
			{
				int	x1 , x2 ;

				TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
				x1 = rectTabPage.left ;
				x2 = rectTabPage.right;
				if( g_bIsFileTreeBarShow == TRUE )
				{
					::GetClientRect( g_hwndFileTreeBar , & rectFileTreeBar );
					x1 += rectFileTreeBar.right + SPLIT_WIDTH ;
					x2 += rectFileTreeBar.right + SPLIT_WIDTH ;
				}
				if( x1 < x && x < x2 && rectTabPage.top < y && y < rectTabPage.bottom )
				{
					SelectTabPageByIndex( nTabPageIndex );
					break;
				}
			}
		}
		else if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hTabPagePopupMenu , 0 , pt.x , pt.y , 0 , g_hwndMainWindow , NULL );
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndScintilla )
	{
		if( p_msg->message == WM_KEYDOWN || p_msg->message == WM_CHAR )
		{
			if( g_pnodeCurrentTabPage->bHexEditMode == TRUE )
			{
				AdujstPositionOnHexEditMode( g_pnodeCurrentTabPage , p_msg );
			}
		}
		else if( p_msg->message == WM_KEYUP )
		{
			int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
			UpdateNavigateBackNextTrace( g_pnodeCurrentTabPage , nCurrentPos );

			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp && g_pnodeCurrentTabPage->bHexEditMode == FALSE )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp( g_pnodeCurrentTabPage , p_msg->wParam , p_msg->lParam );

			UpdateStatusBarLocationInfo();
		} 
	}

	if( p_msg->message == WM_LBUTTONUP )
	{
		nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
		for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
			p = (struct TabPage *)(tci.lParam);
			if( p_msg->hwnd == p->hwndScintilla )
			{
				int nCurrentPos = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
				AddNavigateBackNextTrace( p , nCurrentPos );

				if( p && p->pstDocTypeConfig && p->pstDocTypeConfig->pfuncOnKeyUp && p->bHexEditMode == FALSE )
					p->pstDocTypeConfig->pfuncOnKeyUp( p , p_msg->wParam , p_msg->lParam );

				UpdateStatusBarLocationInfo();

				if( p->bHexEditMode == TRUE )
				{
					AdujstPositionOnHexEditMode( p , p_msg );
				}
			}
		}
	}

	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int		nWidth , nHeight ;
	int		wmId , wmNC ;
	HWND		wmWnd ;
	NMHDR		*lpnmhdr = NULL;
	SCNotification	*lpnotify = NULL ;
	NMTREEVIEW	*lpnmtv = NULL ;
	TOOLTIPTEXT	*pTTT = NULL ;
	struct TabPage	*pnodeTabPage = NULL ;
	PAINTSTRUCT	ps ;
	HDC		hdc ;

	int		nret = 0;

	if( message == WM_COMMAND )
	{
		wmId = LOWORD(wParam) ;
		if( IDM_FILE_OPEN_RECENTLY_HISTORY_BASE <= wmId && wmId <= IDM_FILE_OPEN_RECENTLY_HISTORY_BASE + OPEN_PATHFILENAME_RECENTLY_MAXCOUNT-1 )
		{
			if( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[wmId-IDM_FILE_OPEN_RECENTLY_HISTORY_BASE][0] )
			{
				OpenFileDirectly( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[wmId-IDM_FILE_OPEN_RECENTLY_HISTORY_BASE] );
			}
		}
		if( IDM_VIEW_SWITCH_STYLETHEME_BASE <= wmId && wmId <= IDM_VIEW_SWITCH_STYLETHEME_BASE + VIEW_STYLETHEME_MAXCOUNT-1 )
		{
			if( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[wmId-IDM_VIEW_SWITCH_STYLETHEME_BASE][0] )
			{
				OnViewSwitchStyleTheme( wmId-IDM_VIEW_SWITCH_STYLETHEME_BASE );
			}
		}
		if( IDM_VIEW_SWITCH_FILETYPE_BASE <= wmId && wmId <= IDM_VIEW_SWITCH_FILETYPE_BASE + VIEW_FILETYPE_MAXCOUNT-1 )
		{
			OnViewSwitchFileType( wmId-IDM_VIEW_SWITCH_FILETYPE_BASE );
		}
	}

	switch (message)
	{
	case WM_CREATE:
		nret = OnCreateWindow( hWnd , wParam , lParam ) ;
		if( nret )
			PostQuitMessage(0);
		break;
	case WM_SIZE:
		nWidth = LOWORD( lParam );
		nHeight = HIWORD( lParam );
		OnResizeWindow( hWnd , nWidth , nHeight );
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam) ;
		wmNC = HIWORD(wParam) ;
		wmWnd = (HWND)lParam ;

		// 分析菜单选择:
		if( wmNC == LBN_DBLCLK )
		{
			nWidth = 0 ;
			nHeight = 0 ;
		}

		if( g_pnodeCurrentTabPage && wmWnd == g_pnodeCurrentTabPage->hwndSymbolList && wmNC == LBN_DBLCLK )
		{
			return OnSymbolListDbClick( g_pnodeCurrentTabPage );
		}
		else if( wmNC == BN_CLICKED && wmWnd == g_hwndTabCloseButton )
		{
			if( g_pnodeCloseButtonTabPage )
			{
				return OnCloseFile( g_pnodeCloseButtonTabPage );
			}
		}

		switch (wmId)
		{
		case IDM_FILE_NEW:
			OnNewFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_OPEN:
			OnOpenFile();
			break;
		case IDM_FILE_CLEAN_OPEN_RECENTLY_HISTORY:
			OnCleanOpenRecentlyHistory( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_SAVE:
			OnSaveFile( g_pnodeCurrentTabPage , FALSE );
			break;
		case IDM_FILE_SAVEAS:
			OnSaveFileAs( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_SAVEALL:
			OnSaveAllFiles();
			break;
		case IDM_FILE_CLOSE:
			OnCloseFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_CLOSEALL:
			OnCloseAllFile();
			break;
		case IDM_FILE_CLOSEALL_EXPECT_CURRENT_FILE:
			OnCloseAllExpectCurrentFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_REMOTE_FILESERVERS:
			OnManageRemoteFileServers();
			break;
		case IDM_CRATE_NEWFILE_ON_NEWBOOT:
			OnEnableCreateNewFileOnNewBoot( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_OPENFILES_THAT_OPENNING_ON_EXIT:
			OnOpenFilesThatOpenningOnExit( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_SETREADONLY_AFTER_OPEN:
			OnSetReadOnlyAfterOpenFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_CHECK_UPDATE_WHERE_SELECTTABPAGE:
			OnFileCheckUpdateWhereSelectTabPage( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_PROMPT_WHERE_AUTOUPDATE:
			OnFilePromptWhereAutoUpdate( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_NEWFILE_WINDOWS_EOLS:
			OnFileSetNewFileEols( g_pnodeCurrentTabPage , 0 );
			break;
		case IDM_FILE_NEWFILE_MAC_EOLS:
			OnFileSetNewFileEols( g_pnodeCurrentTabPage , 1 );
			break;
		case IDM_FILE_NEWFILE_UNIX_EOLS:
			OnFileSetNewFileEols( g_pnodeCurrentTabPage , 2 );
			break;
		case IDM_FILE_CONVERT_WINDOWS_EOLS:
			OnFileConvertEols( g_pnodeCurrentTabPage , 0 );
			break;
		case IDM_FILE_CONVERT_MAC_EOLS:
			OnFileConvertEols( g_pnodeCurrentTabPage , 1 );
			break;
		case IDM_FILE_CONVERT_UNIX_EOLS:
			OnFileConvertEols( g_pnodeCurrentTabPage , 2 );
			break;
		case IDM_FILE_NEWFILE_ENCODING_UTF8:
			OnFileSetNewFileEncoding( g_pnodeCurrentTabPage , ENCODING_UTF8 );
			break;
		case IDM_FILE_NEWFILE_ENCODING_GB18030:
			OnFileSetNewFileEncoding( g_pnodeCurrentTabPage , ENCODING_GBK );
			break;
		case IDM_FILE_NEWFILE_ENCODING_BIG5:
			OnFileSetNewFileEncoding( g_pnodeCurrentTabPage , ENCODING_BIG5 );
			break;
		case IDM_FILE_CONVERT_ENCODING_UTF8:
			OnFileConvertEncoding( g_pnodeCurrentTabPage , ENCODING_UTF8 );
			break;
		case IDM_FILE_CONVERT_ENCODING_GB18030:
			OnFileConvertEncoding( g_pnodeCurrentTabPage , ENCODING_GBK );
			break;
		case IDM_FILE_CONVERT_ENCODING_BIG5:
			OnFileConvertEncoding( g_pnodeCurrentTabPage , ENCODING_BIG5 );
			break;
		case IDM_EXIT:
			nret = CheckAllFilesSave() ;
			if( nret )
				break;
			DestroyWindow(hWnd);
			break;
		case IDM_EDIT_UNDO:
			OnUndoEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_REDO:
			OnRedoEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_CUT:
			OnCutText( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPY:
			OnCopyText( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_PASTE:
			OnPasteText( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETE:
			OnDeleteText( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_CUTLINE:
			OnCutLine( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_CUTLINE_AND_PASTELINE:
			OnCutLineAndPasteLine( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPYLINE:
			OnCopyLine( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPYLINE_AND_PASTELINE:
			OnCopyLineAndPasteLine( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPY_FILENAME:
			OnCopyFilename( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPY_PATHNAME:
			OnCopyPathname( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPY_PATHFILENAME:
			OnCopyPathFilename( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_PASTELINE:
			OnPasteLine( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_PASTELINE_UPSTAIRS:
			OnPasteLineUpstairs( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETELINE:
			OnDeleteLine( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETEWHITECHARACTER_AT_LINEHEAD:
			OnDeleteWhiteCharacterAtLineHead( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETEWHITECHARACTER_AT_LINETAIL:
			OnDeleteWhiteCharacterAtLineTail( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETEBLANKLINE:
			OnDeleteBlankLine( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETEBLANKLINE_WITH_WHITECHARACTER:
			OnDeleteBlankLineWithWhiteCharacter( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_JOINLINE:
			OnJoinLine( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_LOWERCASE:
			OnLowerCaseEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_UPPERCASE:
			OnUpperCaseEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_ENABLE_AUTO_ADD_CLOSECHAR:
			OnEditEnableAutoAddCloseChar( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_ENABLE_AUTO_INDENTATION:
			OnEditEnableAutoIdentation( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_BASE64_ENCODING:
			OnEditBase64Encoding( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_BASE64_DECODING:
			OnEditBase64Decoding( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_MD5:
			OnEditMd5( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_SHA1:
			OnEditSha1( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_SHA256:
			OnEditSha256( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_3DES_CBC_ENCRYPTO:
			OnEdit3DesCbcEncrypto( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_3DES_CBC_DECRYPTO:
			OnEdit3DesCbcDecrypto( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FIND:
			OnSearchFind( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FINDPREV:
			OnSearchFindPrev( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FINDNEXT:
			OnSearchFindNext( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FOUNDLIST:
			OnSearchFoundList( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_REPLACE:
			OnSearchReplace( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_SELECTALL:
			OnSelectAll( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_SELECTWORD:
			OnSelectWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_SELECTLINE:
			OnSelectLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_LEFT_CHARGROUP:
			OnAddSelectLeftCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_RIGHT_CHARGROUP:
			OnAddSelectRightCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_LEFT_WORD:
			OnAddSelectLeftWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_RIGHT_WORD:
			OnAddSelectRightWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_TOP_BLOCKFIRSTLINE:
			OnAddSelectTopBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_BOTTOM_BLOCKFIRSTLINE:
			OnAddSelectBottomBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_LEFT_CHARGROUP:
			OnMoveLeftCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_RIGHT_CHARGROUP:
			OnMoveRightCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_LEFT_WORD:
			OnMoveLeftWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_RIGHT_WORD:
			OnMoveRightWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_TOP_BLOCKFIRSTLINE:
			OnMoveTopBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_BOTTOM_BLOCKFIRSTLINE:
			OnMoveBottomBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_TOGGLE_BOOKMARK:
			OnSearchToggleBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADD_BOOKMARK:
			OnSearchAddBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_REMOVE_BOOKMARK:
			OnSearchRemoveBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_REMOVE_ALL_BOOKMARKS:
			OnSearchRemoveAllBookmarks( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_PREV_BOOKMARK:
			OnSearchGotoPrevBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_NEXT_BOOKMARK:
			OnSearchGotoNextBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_PREV_BOOKMARK_IN_ALL_FILES:
			OnSearchGotoPrevBookmarkInAllFiles( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_NEXT_BOOKMARK_IN_ALL_FILES:
			OnSearchGotoNextBookmarkInAllFiles( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTOLINE:
			OnSearchGotoLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE:
			OnSearchNavigateBackPrev_InThisFile();
			break;
		case IDM_SEARCH_NAVIGATEBACK_PREV_IN_ALL_FILES:
			OnSearchNavigateBackPrev_InAllFiles();
			break;
		case IDM_SEARCH_MULTISELECT_README:
			::MessageBox( NULL , "使用快捷键Ctrl+MouseLButtonClick多位置定位、Ctrl+MouseLButtonDown+MouseMove+MouseLButtonUp多个文本块选择，支持多位置联动操作：输入、退格键、删除、复制等" , "即时帮助" , MB_ICONINFORMATION | MB_OK);
			break;
		case IDM_SEARCH_COLUMNSELECT_README:
			::MessageBox( NULL , "使用快捷键Alt+MouseLButtonDown+MouseMove或Alt+Shift+Left/Right/Up/Down进行列选择，支持多行联动操作：输入、退格键、删除、复制等" , "即时帮助" , MB_ICONINFORMATION | MB_OK);
			break;
		case IDM_VIEW_FILETREE:
			OnViewFileTree( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_MODIFY_STYLETHEME:
			OnViewModifyThemeStyle();
			break;
		case IDM_VIEW_COPYNEW_STYLETHEME:
			OnViewCopyNewThemeStyle();
			break;
		case IDM_VIEW_HEXEDIT_MODE:
			OnViewHexEditMode( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_WRAPLINE_MODE:
			OnViewWrapLineMode( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ENABLE_WINDOWS_VISUAL_STYLES:
			OnViewEnableWindowsVisualStyles();
			break;
		case IDM_VIEW_TAB_WIDTH:
			OnViewTabWidth( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ONKEYDOWN_TAB_CONVERT_SPACES:
			OnViewOnKeydownTabConvertSpaces( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_LINENUMBER_VISIABLE:
			OnViewLineNumberVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_BOOKMARK_VISIABLE:
			OnViewBookmarkVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_WHITESPACE_VISIABLE:
			OnViewWhiteSpaceVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_NEWLINE_VISIABLE:
			OnViewNewLineVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_INDENTATIONGUIDES_VISIABLE:
			OnViewIndentationGuidesVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ZOOMOUT:
			OnViewZoomOut( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ZOOMIN:
			OnViewZoomIn( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ZOOMRESET:
			OnViewZoomReset( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_VISIABLE:
			OnSourceCodeBlockFoldVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_TOGGLE:
			OnSourceCodeBlockFoldToggle( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_CONTRACT:
			OnSourceCodeBlockFoldContract( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_EXPAND:
			OnSourceCodeBlockFoldExpand( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_CONTRACTALL:
			OnSourceCodeBlockFoldContractAll( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_EXPANDALL:
			OnSourceCodeBlockFoldExpandAll( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_GOTODEF:
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , VK_F11 , lParam );
			break;
		case IDM_SOURCECODE_SET_RELOAD_SYMBOLLIST_OR_TREE_INTERVAL:
			OnSetReloadSymbolListOrTreeInterval( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_ENABLE_AUTOCOMPLETEDSHOW:
			OnSourceCodeEnableAutoCompletedShow( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_AUTOCOMPLETEDSHOW_AFTER_INPUT_CHARACTERS:
			OnSourceCodeAutoCompletedShowAfterInputCharacters( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_ENABLE_CALLTIPSHOW:
			OnSourceCodeEnableCallTipShow( g_pnodeCurrentTabPage );
			break;
		case IDM_DATABASE_INSERT_DATABASE_CONNECTION_CONFIG:
			OnInsertDataBaseConnectionConfig( g_pnodeCurrentTabPage );
			break;
		case IDM_DATABASE_EXECUTE_SQL:
		case IDM_DATABASE_EXECUTE_REDIS_COMMAND:
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , VK_F5 , 0 );
			break;
		case IDM_DATABASE_AUTO_SELECT_CURRENT_SQL_AND_EXECUTE:
		case IDM_DATABASE_AUTO_SELECT_CURRENT_REDIS_COMMAND_AND_EXECUTE:
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , VK_F5 , VK_CONTROL );
			break;
		case IDM_DATABASE_INSERT_REDIS_CONNECTION_CONFIG:
			OnInsertRedisConnectionConfig( g_pnodeCurrentTabPage );
			break;
		case IDM_ENV_FILE_POPUPMENU:
			OnEnvFilePopupMenu();
			break;
		case IDM_ENV_DIRECTORY_POPUPMENU:
			OnEnvDirectoryPopupMenu();
			break;
		case IDM_ENV_SET_PROCESSFILE_CMD:
			OnEnvSetProcessFileCommand();
			break;
		case IDM_ENV_EXECUTE_PROCESSFILE_CMD:
			OnEnvExecuteProcessFileCommand( g_pnodeCurrentTabPage );
			break;
		case IDM_ENV_SET_PROCESSTEXT_CMD:
			OnEnvSetProcessTextCommand();
			break;
		case IDM_ENV_EXECUTE_PROCESSTEXT_CMD:
			OnEnvExecuteProcessTextCommand( g_pnodeCurrentTabPage );
			break;
		case IDM_CHANGELOG:
		{
			char	acChangeLogPathFilename[ MAX_PATH ] ;
			memset( acChangeLogPathFilename , 0x00 , sizeof(acChangeLogPathFilename) );
			snprintf( acChangeLogPathFilename , sizeof(acChangeLogPathFilename)-1 , "%s\\ChangeLog-CN" , g_acModulePathName );
			OpenFileDirectly( acChangeLogPathFilename );
		}
		break;
		case IDM_ABOUT:
			DialogBox(g_hAppInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_MOUSEMOVE:
			if( g_bIsFileTreeBarShow == TRUE )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsFileTreeBarResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( g_rectFileTreeBar.top <= y && y <= g_rectFileTreeBar.bottom && g_rectFileTreeBar.right < x && x < g_rectTabPages.left )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					if( g_bIsFileTreeBarHoverResizing == FALSE )
						g_bIsFileTreeBarHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsFileTreeBarHoverResizing == TRUE )
						g_bIsFileTreeBarHoverResizing = FALSE ;
				}

				if( g_bIsFileTreeBarResizing == TRUE )
				{
					g_stEditUltraMainConfig.nFileTreeBarWidth = x - g_rectFileTreeBar.left - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nFileTreeBarWidth < FILETREEBAR_WIDTH_MIN )
						g_stEditUltraMainConfig.nFileTreeBarWidth = FILETREEBAR_WIDTH_MIN ;

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			if( g_pnodeCurrentTabPage && g_bIsTabPageMoving == TRUE )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				RECT rectTabPage ;
				for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
				{
					TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
					if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
					{
						::SetCursor( LoadCursor(NULL,IDC_SIZEALL) );
					}
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndSymbolList )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsFunctionListResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( g_pnodeCurrentTabPage->rectSymbolList.top <= y && y <= g_pnodeCurrentTabPage->rectSymbolList.bottom && g_pnodeCurrentTabPage->rectScintilla.right < x && x < g_pnodeCurrentTabPage->rectSymbolList.left )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					g_bIsFunctionListHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsFunctionListHoverResizing == TRUE )
						g_bIsFunctionListHoverResizing = FALSE ;
				}

				if( g_bIsFunctionListResizing == TRUE )
				{
					g_stEditUltraMainConfig.nSymbolListWidth = g_pnodeCurrentTabPage->rectSymbolList.right - x - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nSymbolListWidth < SYMBOLLIST_WIDTH_MIN )
						g_stEditUltraMainConfig.nSymbolListWidth = SYMBOLLIST_WIDTH_MIN ;

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndSymbolTree )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsTreeViewResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( g_pnodeCurrentTabPage->rectSymbolTree.top <= y && y <= g_pnodeCurrentTabPage->rectSymbolTree.bottom && g_pnodeCurrentTabPage->rectScintilla.right < x && x < g_pnodeCurrentTabPage->rectSymbolTree.left )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					g_bIsTreeViewHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsTreeViewHoverResizing == TRUE )
						g_bIsTreeViewHoverResizing = FALSE ;
				}

				if( g_bIsTreeViewResizing == TRUE )
				{
					g_stEditUltraMainConfig.nSymbolTreeWidth = g_pnodeCurrentTabPage->rectSymbolTree.right - x - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nSymbolTreeWidth < TREEVIEW_WIDTH_MIN )
						g_stEditUltraMainConfig.nSymbolTreeWidth = TREEVIEW_WIDTH_MIN ;

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndQueryResultEdit )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsSqlQueryResultEditResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
				}
				else if( g_pnodeCurrentTabPage->rectQueryResultEdit.left <= x && x <= g_pnodeCurrentTabPage->rectQueryResultEdit.right && g_pnodeCurrentTabPage->rectScintilla.bottom < y && y < g_pnodeCurrentTabPage->rectQueryResultEdit.top )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
					g_bIsSqlQueryResultEditHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsSqlQueryResultEditHoverResizing == TRUE )
						g_bIsSqlQueryResultEditHoverResizing = FALSE ;
				}

				if( g_bIsSqlQueryResultEditResizing == TRUE )
				{
					g_stEditUltraMainConfig.nSqlQueryResultEditHeight = g_pnodeCurrentTabPage->rectQueryResultEdit.bottom - y - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nSqlQueryResultEditHeight < SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN )
						g_stEditUltraMainConfig.nSqlQueryResultEditHeight = SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN ;

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndQueryResultTable )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsSqlQueryResultListViewResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
				}
				else if( g_pnodeCurrentTabPage->rectQueryResultListView.left <= x && x <= g_pnodeCurrentTabPage->rectQueryResultListView.right && g_pnodeCurrentTabPage->rectQueryResultEdit.bottom < y && y < g_pnodeCurrentTabPage->rectQueryResultListView.top )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
					g_bIsSqlQueryResultListViewHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsSqlQueryResultListViewHoverResizing == TRUE )
						g_bIsSqlQueryResultListViewHoverResizing = FALSE ;
				}

				if( g_bIsSqlQueryResultListViewResizing == TRUE )
				{
					int	nListViewHeight = g_pnodeCurrentTabPage->rectQueryResultListView.bottom - y - SPLIT_WIDTH/2 ;
					if( nListViewHeight >= SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN )
					{
						g_stEditUltraMainConfig.nSqlQueryResultListViewHeight = nListViewHeight ;
					}

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) - g_nToolBarHeight ;
				RECT rectClient ;
				int nTabPageIndex ;

				::GetClientRect( g_hwndMainWindow , & rectClient );

				int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				RECT rectTabPage , rectFileTreeBar ;
				for( nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
				{
					int	x1 , x2 ;

					TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
					x1 = rectTabPage.left ;
					x2 = rectTabPage.right;
					if( g_bIsFileTreeBarShow == TRUE )
					{
						::GetClientRect( g_hwndFileTreeBar , & rectFileTreeBar );
						x1 += rectFileTreeBar.right + SPLIT_WIDTH ;
						x2 += rectFileTreeBar.right + SPLIT_WIDTH ;
					}
					if( x1 < x && x < x2 && rectTabPage.top < y && y < rectTabPage.bottom && x < rectClient.right - 50 )
					{
						g_pnodeCloseButtonTabPage = GetTabPageByIndex(nTabPageIndex) ;
						::SetWindowPos( g_hwndTabCloseButton , HWND_TOP , x2-TABCLOSEBUTTON_WIDTH+13 , rectTabPage.top+4+g_nToolBarHeight , TABCLOSEBUTTON_WIDTH , TABCLOSEBUTTON_HEIGHT , SWP_SHOWWINDOW );
						::ShowWindow( g_hwndTabCloseButton , SW_SHOW );
						::InvalidateRect( g_hwndTabCloseButton , NULL , TRUE );
						::UpdateWindow( g_hwndTabCloseButton );
						break;
					}
				}
				if( nTabPageIndex >= nTabPagesCount )
				{
					g_pnodeCloseButtonTabPage = NULL ;
					::SetWindowPos( g_hwndTabCloseButton , HWND_BOTTOM , 0 , 0 , 0 , 0 , SWP_HIDEWINDOW );
					::ShowWindow( g_hwndTabCloseButton , SW_HIDE );
					::InvalidateRect( g_hwndTabCloseButton , NULL , TRUE );
					::UpdateWindow( g_hwndTabCloseButton );
				}
			}

			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}

		break;
	case WM_NOTIFY:
		lpnmhdr = (NMHDR*)lParam ;
		lpnotify = (SCNotification*)lParam ;
		lpnmtv = (NMTREEVIEW*)lParam ;
		pTTT = (TOOLTIPTEXT *)lParam;

		switch (lpnmhdr->code)
		{
		case SCN_CHARADDED:
			OnCharAdded( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) , lpnotify );
			break;
		case SCN_MODIFIED:
			if( lpnotify->linesAdded )
			{
				AutosetLineNumberMarginWidth( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
			}
			break;
		case TCN_SELCHANGE:
			OnSelectChange();
			break;
		case SCN_SAVEPOINTREACHED:
			OnSavePointReached( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
			break;
		case SCN_SAVEPOINTLEFT:
			OnSavePointLeft( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
			break;
		case SCN_MARGINCLICK:
			OnMarginClick( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) , lpnotify );
			break;
		case SCN_UPDATEUI:
			if( ( lpnotify->updated & SC_UPDATE_SELECTION ) )
			{
				OnEditorTextSelected( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
				UpdateStatusBarSelectionInfo();
			}
			break;
		case TVN_ITEMEXPANDING:
			if( lpnmhdr->hwndFrom == g_hwndFileTree )
			{
				OnFileTreeNodeExpanding( lpnmtv );
			}
			break;
		case NM_DBLCLK:
			if( lpnmhdr->hwndFrom == g_hwndFileTree )
			{
				nret = OnFileTreeNodeDbClick() ;
				if( nret == 1 )
					return DefWindowProc(hWnd, message, wParam, lParam);
			}
			else if( g_pnodeCurrentTabPage && lpnmhdr->hwndFrom == g_pnodeCurrentTabPage->hwndSymbolTree )
			{
				if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree )
					g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree( g_pnodeCurrentTabPage );
				return 0;
			}
			break;
		case TTN_NEEDTEXT:
			pnodeTabPage = GetTabPageByIndex( (int)(pTTT->hdr.idFrom) ) ;
			if( pnodeTabPage )
			{
				memset( pTTT->szText , 0x00 , sizeof(pTTT->szText) );
				if( (int)(pTTT->hdr.idFrom) <= 8 )
					snprintf( pTTT->szText , sizeof(pTTT->szText)-1 , "%.68s - (Alt+%d)" , pnodeTabPage->acPathFilename , (int)(pTTT->hdr.idFrom)+1 );
				else
					snprintf( pTTT->szText , sizeof(pTTT->szText)-1 , "%.68s" , pnodeTabPage->acPathFilename );
			}
			break;
		}

		break;
		/*
		case WM_DRAWITEM:
		{
		LPDRAWITEMSTRUCT pDIS = (LPDRAWITEMSTRUCT)lParam;
		if( pDIS->hwndItem == g_hwndTabPages )
		{
		}
		}
		break;
		case WM_MEASUREITEM:
		{
		LPDRAWITEMSTRUCT pDIS = (LPDRAWITEMSTRUCT)lParam;
		if( pDIS->hwndItem == g_hwndTabPages )
		{
		}
		}
		break;
		*/
	case WM_COPYDATA:
	{
		COPYDATASTRUCT	*cpd ;
		cpd = (COPYDATASTRUCT*)lParam ;
		char acPathFilename[MAX_PATH] = "" ;
		strncpy( acPathFilename , (char*)(cpd->lpData) , MIN(sizeof(acPathFilename)-1,cpd->cbData) );

		if( acPathFilename[strlen(acPathFilename)-1] != '\\' )
			OpenFilesDirectly( acPathFilename );
		else
			LocateDirectory( acPathFilename );
	}

	/*
	{
		HWND hForeWnd = NULL; 
		DWORD dwForeID; 
		DWORD dwCurID; 

		hForeWnd = ::GetForegroundWindow(); 
		dwCurID = ::GetCurrentThreadId(); 
		dwForeID = ::GetWindowThreadProcessId( hForeWnd, NULL ); 
		::AttachThreadInput( dwCurID, dwForeID, TRUE); 
		::SetWindowPos( g_hwndMainWindow, HWND_TOP, 0,0,0,0, SWP_NOSIZE|SWP_NOMOVE ); 
		::SetForegroundWindow( g_hwndMainWindow ); 
		::AttachThreadInput( dwCurID, dwForeID, FALSE);
	}
	*/
	SwitchToThisWindow( g_hwndMainWindow , TRUE );

	break;
	case WM_PAINT:
		hdc = BeginPaint( hWnd, & ps );
		/*
		RECT rect ;
		::GetClientRect( hWnd , & rect );
		::FillRect( hdc , & rect , g_brushCommonControlDarkColor );
		*/
		EndPaint( hWnd, & ps);
		break;
	case WM_CTLCOLORBTN:
		if( (HWND)lParam == g_hwndTabCloseButton && g_pnodeCloseButtonTabPage )
		{
			HWND btn = (HWND)lParam ;
			HDC hdc = (HDC)wParam ;
			RECT rect;
			::GetClientRect( btn , & rect );
			::SetTextColor( hdc , ::GetSysColor(COLOR_BTNFACE) );
			::SetBkColor( hdc , ::GetSysColor(COLOR_HIGHLIGHT) );
			::SetBkMode( hdc , TRANSPARENT);
			::DrawText( hdc , "X" , 1 , & rect , DT_CENTER|DT_VCENTER|DT_SINGLELINE );
			return (LRESULT)g_brushTabCloseButton;
		}
	case WM_CTLCOLORLISTBOX:
	case WM_CTLCOLOREDIT:
	{
		HDC hdc = (HDC)wParam ;
		::SetTextColor( hdc , g_pstWindowTheme->stStyleTheme.text.color );
		::SetBkColor( hdc , g_pstWindowTheme->stStyleTheme.text.bgcolor );
		::SetBkMode( hdc , TRANSPARENT);
		return (LRESULT)g_brushCommonControlDarkColor;
	}
	break;
	case WM_ACTIVATE:
		if( g_pnodeCurrentTabPage )
		{
			if( hWnd == g_hwndMainWindow )
			{
				OnSelectChange();
			}
		}
		break;
	case WM_LBUTTONDOWN:
		if( g_bIsFileTreeBarHoverResizing == TRUE )
			g_bIsFileTreeBarResizing = TRUE ;

		if( g_bIsFunctionListHoverResizing == TRUE )
			g_bIsFunctionListResizing = TRUE ;

		if( g_bIsTreeViewHoverResizing == TRUE )
			g_bIsTreeViewResizing = TRUE ;

		if( g_bIsSqlQueryResultEditHoverResizing == TRUE )
			g_bIsSqlQueryResultEditResizing = TRUE ;

		if( g_bIsSqlQueryResultListViewHoverResizing == TRUE )
			g_bIsSqlQueryResultListViewResizing = TRUE ;

		break;
	case WM_LBUTTONUP:
		if( g_bIsFileTreeBarResizing == TRUE )
		{
			g_bIsFileTreeBarResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		if( g_bIsFunctionListResizing == TRUE )
		{
			g_bIsFunctionListResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		if( g_bIsTreeViewResizing == TRUE )
		{
			g_bIsTreeViewResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		if( g_bIsSqlQueryResultEditHoverResizing == TRUE )
		{
			g_bIsSqlQueryResultEditResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		if( g_bIsSqlQueryResultListViewHoverResizing == TRUE )
		{
			g_bIsSqlQueryResultListViewResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		break;
	case WM_DROPFILES:
		if( wParam )
		{
			OnOpenDropFile( (HDROP)wParam );
		}
		break;
	case WM_CLOSE:
		if( hWnd == g_hwndMainWindow )
		{
			if( g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit == TRUE )
			{
				FillAllOpenFilesOnBoot();

				memset( g_stEditUltraMainConfig.acActiveFileOnBoot , 0x00 , sizeof(g_stEditUltraMainConfig.acActiveFileOnBoot) );
				if( g_pnodeCurrentTabPage )
					strncpy( g_stEditUltraMainConfig.acActiveFileOnBoot , g_pnodeCurrentTabPage->acPathFilename , sizeof(g_stEditUltraMainConfig.acActiveFileOnBoot)-1 );
			}

			nret = CheckAllFilesSave() ;
			if( nret )
				break;
		}
		return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		CenterWindow( hDlg , g_hwndMainWindow );
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
#if 0
			char buf[1024];
			memset(buf,0x00,sizeof(buf));
			g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_STYLEGETFONT , STYLE_DEFAULT , (sptr_t)buf );
			InfoBox(buf);
#endif
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
