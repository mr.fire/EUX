#include "framework.h"

HWND	g_hwndFileTreeBar = NULL ;
BOOL	g_bIsFileTreeBarShow = FALSE ;
RECT	g_rectFileTreeBar = { 0 } ;

HWND	g_hwndFileTree = NULL ;

HMENU	g_hFileTreeFilePopupMenu ;
HMENU	g_hFileTreeDirectoryPopupMenu ;

int	nFileTreeImageDrive ;
int	nFileTreeImageOpenFold ;
int	nFileTreeImageClosedFold ;
int	nFileTreeImageTextFile ;
int	nFileTreeImageGeneralFile ;
int	nFileTreeImageExecFile ;

static BOOL LoadTreeViewImageLists(HWND hwnd) 
{ 
	HIMAGELIST himl;  // handle to image list 
	HBITMAP hbmp;     // handle to bitmap 

	if ((himl = ImageList_Create(16, 16, ILC_COLOR, 6, 0)) == NULL) 
		return FALSE; 

	// Add the open file, closed file, and document bitmaps. 
	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_DRIVE)); 
	nFileTreeImageDrive = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_OPENFOLD)); 
	nFileTreeImageOpenFold = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_CLSDFOLD)); 
	nFileTreeImageClosedFold = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_TXT)); 
	nFileTreeImageGeneralFile = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_DOC)); 
	nFileTreeImageTextFile = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	hbmp = LoadBitmap(g_hAppInstance, MAKEINTRESOURCE(IDB_EXE)); 
	nFileTreeImageExecFile = ImageList_Add(himl, hbmp, (HBITMAP)NULL); 
	DeleteObject(hbmp); 

	// Fail if not all of the images were added. 
	if (ImageList_GetImageCount(himl) < 6) 
		return FALSE; 

	// Associate the image list with the tree-view control. 
	TreeView_SetImageList(hwnd, himl, TVSIL_NORMAL); 

	return TRUE; 
}

int CreateFileTreeBar( HWND hWnd )
{
	RECT		rectMainClient ;
	TCITEM		tci ;

	BOOL		bret ;
	int		nret ;

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_COOL_CLASSES | ICC_BAR_CLASSES ;
	bret = InitCommonControlsEx( & icex ) ;
	if (bret != TRUE)
	{
		::MessageBox(NULL, TEXT("不能注册文件树ReBar控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	GetClientRect( hWnd , & rectMainClient );
	memcpy( & g_rectFileTreeBar , & rectMainClient , sizeof(RECT) );
	g_rectFileTreeBar.left = rectMainClient.left ;
	g_rectFileTreeBar.right = g_rectFileTreeBar.left + FILETREEBAR_WIDTH_DEFAULT ;
	g_rectFileTreeBar.top = rectMainClient.top ;
	g_rectFileTreeBar.bottom = rectMainClient.bottom ;
	g_hwndFileTreeBar = ::CreateWindow( WC_TABCONTROL , NULL , WS_CHILD , g_rectFileTreeBar.left , g_rectFileTreeBar.top , g_rectFileTreeBar.right - g_rectFileTreeBar.left , g_rectFileTreeBar.bottom-g_rectFileTreeBar.top , hWnd , NULL , g_hAppInstance , NULL ) ;
	if( g_hwndFileTreeBar == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建TabControl控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -2;
	}

	SendMessage( g_hwndFileTreeBar , WM_SETFONT , (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);
	TabCtrl_SetPadding( g_hwndFileTreeBar , 20 , 5 );

	tci.mask = TCIF_TEXT ;
	tci.pszText = (char*)"文件资源管理器" ;
	nret = TabCtrl_InsertItem( g_hwndFileTreeBar , 0 , & tci ) ;
	if( nret == -1 )
	{
		::MessageBox(NULL, TEXT("TabCtrl_InsertItem失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return NULL;
	}

	// hwndFileTree = ::CreateWindowEx( WS_EX_STATICEDGE , WC_TREEVIEW , NULL , WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP , rectFileTreeBar.left , rectFileTreeBar.top+TABS_HEIGHT_DEFAULT , rectFileTreeBar.right-rectFileTreeBar.left , rectFileTreeBar.bottom-rectFileTreeBar.top-TABS_HEIGHT_DEFAULT , hWnd , NULL , g_hAppInstance , NULL ) ; 
	g_hwndFileTree = ::CreateWindow( WC_TREEVIEW , NULL , WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP|TVS_SHOWSELALWAYS , g_rectFileTreeBar.left , g_rectFileTreeBar.top+TABS_HEIGHT_DEFAULT , g_rectFileTreeBar.right-g_rectFileTreeBar.left , g_rectFileTreeBar.bottom-g_rectFileTreeBar.top-TABS_HEIGHT_DEFAULT , hWnd , NULL , g_hAppInstance , NULL ) ; 
	if( g_hwndFileTree == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建文件树TreeView控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -2;
	}

	/*
	::SendMessage( hwndFileTree , TVM_SETTEXTCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
	::SendMessage( hwndFileTree , TVM_SETLINECOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
	::SendMessage( hwndFileTree , TVM_SETBKCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.bgcolor );
	*/

	LoadTreeViewImageLists( g_hwndFileTree );

	LoadDrivesToFileTree( g_hwndFileTree );

	LoadRemoteDriversToFileTree( g_hwndFileTree );

	::ShowWindow( g_hwndFileTreeBar , SW_HIDE);
	::UpdateWindow( g_hwndFileTreeBar );
	::ShowWindow( g_hwndFileTree , SW_HIDE);
	::UpdateWindow( g_hwndFileTree );

	g_hFileTreeDirectoryPopupMenu = ::LoadMenu( g_hAppInstance , MAKEINTRESOURCE(IDR_FILETREE_DIRECTORY_POPUPMENU) ) ;
	if( g_hFileTreeDirectoryPopupMenu == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建目录树右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	g_hFileTreeDirectoryPopupMenu = GetSubMenu( g_hFileTreeDirectoryPopupMenu , 0 ) ;

	g_hFileTreeFilePopupMenu = ::LoadMenu( g_hAppInstance , MAKEINTRESOURCE(IDR_FILETREE_FILE_POPUPMENU) ) ;
	if( g_hFileTreeFilePopupMenu == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建文件树右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	g_hFileTreeFilePopupMenu = GetSubMenu( g_hFileTreeFilePopupMenu , 0 ) ;

	HMENU hMenu = GetMenu( g_hwndMainWindow ) ;
	CheckMenuItem(hMenu, IDM_VIEW_FILETREE, MF_UNCHECKED);

	return 0;
}

void AdjustFileTreeBox( RECT *rectFileTreeBar , RECT *rectFileTree )
{
	rectFileTree->left = FILETREE_MARGIN_LEFT ;
	rectFileTree->right = rectFileTreeBar->right - FILETREE_MARGIN_RIGHT ;
	rectFileTree->top = rectFileTreeBar->top + TABS_HEIGHT_DEFAULT + FILETREE_MARGIN_TOP ;
	rectFileTree->bottom = rectFileTreeBar->bottom - FILETREE_MARGIN_BOTTOM ;
	return;
}

static struct TreeViewData *AllocTreeViewData( struct RemoteFileServer *pstRemoteFileServer , char *acPathName , char *acPathFilename , char *acFilename , BOOL bIsLoadedCompleted , int nImageIndex )
{
	struct TreeViewData	*tvd = NULL ;

	tvd = (struct TreeViewData *)malloc( sizeof(struct TreeViewData) ) ;
	if( tvd == NULL )
		return NULL;
	memset( tvd , 0x00 , sizeof(struct TreeViewData) );

	tvd->pstRemoteFileServer = pstRemoteFileServer ;
	strcpy( tvd->acPathName , acPathName );
	strcpy( tvd->acPathFilename , acPathFilename );
	strcpy( tvd->acFilename , acFilename );
	tvd->bIsLoadedCompleted = bIsLoadedCompleted ;
	tvd->nImageIndex = nImageIndex ;

	return tvd;
}

struct TreeViewData *GetTreeViewDataFromHTREEITEM( HTREEITEM hti )
{
	TVITEM		tviChild ;
	BOOL		bret ;

	memset( & tviChild , 0x00 , sizeof(TVITEM) );
	tviChild.mask = TVIF_HANDLE ;
	tviChild.hItem = hti ;
	bret = TreeView_GetItem( g_hwndFileTree , & tviChild ) ;
	if( bret != TRUE )
		return NULL;

	struct TreeViewData *tvd = (struct TreeViewData *)(tviChild.lParam) ;
	return tvd;
}

struct TreeViewData *AddFileTreeNode( HWND hwnd , HTREEITEM parent , int nImageIndex , struct RemoteFileServer *pstRemoteFileServer , char *acPathName , char *acPathFilename , char *acFilename , BOOL bIsLoadedCompleted )
{
	TVITEM			tvi ;
	TVINSERTSTRUCT		tvis;
	struct TreeViewData	*tvd = NULL ;

	tvd = AllocTreeViewData( pstRemoteFileServer , acPathName , acPathFilename , acFilename , bIsLoadedCompleted , nImageIndex ) ;
	if( tvd == NULL )
		return NULL;

	tvi.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_DI_SETITEM | TVIF_PARAM ;
	tvi.iImage = tvi.iSelectedImage = nImageIndex ;
	tvi.pszText = acFilename ;
	tvi.lParam = (LPARAM)tvd ;
	tvis.hParent = parent ;
	tvis.item = tvi;
	tvis.hInsertAfter = (parent==TVI_ROOT?TVI_LAST:TVI_SORT) ;
	tvd->hti = TreeView_InsertItem( hwnd , & tvis ) ;
	
	return tvd;
}

int AppendFileTreeNodeChildren( HWND hwnd , struct TreeViewData *tvdParent )
{
	char			acFindPathFilename[ MAX_PATH+1 ] ;
	WIN32_FIND_DATA		stFindFileData ;
	HANDLE			hFindFile ;
	char			acPathFilename[ MAX_PATH ] ;
	struct TreeViewData	*tvd = NULL ;

	sprintf( acFindPathFilename , "%s/*" , tvdParent->acPathFilename );
	hFindFile = ::FindFirstFile( acFindPathFilename , & stFindFileData ) ;
	if( hFindFile == INVALID_HANDLE_VALUE )
		return 0;

	do
	{
		if( strcmp(stFindFileData.cFileName,".") == 0 || strcmp(stFindFileData.cFileName,"..") == 0 )
			continue;

		if( stFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			_snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s/%s" , tvdParent->acPathFilename , stFindFileData.cFileName ) ;
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageClosedFold , NULL , tvdParent->acPathFilename , acPathFilename , stFindFileData.cFileName , FALSE ) ;
			if( tvd == NULL )
			{
				::FindClose( hFindFile );
				return -1;
			}
		}
		else
		{
			_snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s/%s" , tvdParent->acPathFilename , stFindFileData.cFileName ) ;
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageTextFile , NULL , tvdParent->acPathFilename , acPathFilename , stFindFileData.cFileName , TRUE ) ;
			if( tvd == NULL )
			{
				::FindClose( hFindFile );
				return -1;
			}
		}
	}
	while( ::FindNextFile( hFindFile , & stFindFileData ) );

	::FindClose( hFindFile );

	tvdParent->bIsLoadedCompleted = TRUE ;

	return 0;
}

int LoadDrivesToFileTree( HWND hwnd )
{
	char			acDrivenames[ MAX_PATH ] ;
	char			*pcDrivename = NULL ;
	size_t			nDriveNameLength ;
	char			*p = NULL ;
	struct TreeViewData	*tvd = NULL ;
	int			nret = 0 ;

	::GetLogicalDriveStrings( sizeof(acDrivenames)-1 , acDrivenames ) ;
	pcDrivename = acDrivenames ;
	while( pcDrivename[0] )
	{
		nDriveNameLength = strlen(pcDrivename) ;

		p = strchr( pcDrivename , '\\' ) ;
		if( p )
			*(p) = '\0' ;
		tvd = AddFileTreeNode( hwnd , TVI_ROOT , nFileTreeImageDrive , NULL , pcDrivename , pcDrivename , pcDrivename , TRUE ) ;
		if( tvd == NULL )
			return -1;

		nret = AppendFileTreeNodeChildren( hwnd , tvd ) ;
		if( nret )
			break;

		pcDrivename += nDriveNameLength + 1 ;
	}

	return 0;
}

static size_t ReadRemoteDirectory(void *buffer, size_t size, size_t nmemb, void *stream)
{
	struct RemoteFileBuffer	*rfb = (struct RemoteFileBuffer *)stream ;
	size_t			nTransferLen ;
	size_t			nReadLen ;

	nReadLen = nTransferLen = size * nmemb ;
	if( nReadLen > rfb->remain_len )
		nReadLen = rfb->remain_len ;

	memcpy( rfb->buf + rfb->str_len , buffer , nReadLen );
	rfb->str_len += nReadLen ;
	rfb->remain_len -= nReadLen ;

	return nTransferLen;
}

int AppendRemoteFileTreeNodeChildren( HWND hwnd , struct TreeViewData *tvdParent , CURL *curl )
{
	struct RemoteFileBuffer	*rfb = NULL ;
	char			userpwd[ 256 ] ;
	CURLcode		res ;
	char			*p1 = NULL ;
	char			*p2 = NULL ;
	char			chFileType ;
	char			acPathFilename[ MAX_PATH ] ;
	char			acFilename[ MAX_PATH ] ;
	struct TreeViewData	*tvd = NULL ;
	int			nret = 0 ;

	if( tvdParent->pstRemoteFileServer->acLoginPass[0] == '\0' )
	{
		nret = InputBox( g_hwndMainWindow , "请输入用户密码：" , "输入窗口" , 0 , tvdParent->pstRemoteFileServer->acLoginPass , sizeof(tvdParent->pstRemoteFileServer->acLoginPass)-1 ) ;
		if( nret == IDOK )
		{
			if( tvdParent->pstRemoteFileServer->acLoginPass[0] == '\0' )
			{
				return 1;
			}
		}
		else if( nret == IDCANCEL )
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	rfb = (struct RemoteFileBuffer *)malloc( sizeof(struct RemoteFileBuffer) ) ;
	if( rfb == NULL )
	{
		::MessageBox(NULL, TEXT("不能申请内存以存放远程文件列表缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return 1;
	}
	memset( rfb , 0x00 , sizeof(struct RemoteFileBuffer) );
	rfb->remain_len = REMOTE_FILE_BUFFER_SIZE_DEFAULT - 2 ;

	memset( userpwd , 0x00 , sizeof(userpwd) );
	snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdParent->pstRemoteFileServer->acLoginUser , tvdParent->pstRemoteFileServer->acLoginPass );
	curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
	curl_easy_setopt( curl , CURLOPT_CUSTOMREQUEST , "ls" );
	curl_easy_setopt( curl , CURLOPT_WRITEDATA , rfb );
	curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
	res = curl_easy_perform( curl ) ;
	if( res == CURLE_REMOTE_ACCESS_DENIED )
	{
		free( rfb );
		return 0;
	}
	else if( res != CURLE_OK )
	{
		::ErrorBox( "连接远程文件服务器失败[%s]" , curl_easy_strerror(res) );
		free( rfb );
		if( tvdParent->pstRemoteFileServer->bConfigLoginPass == FALSE )
			memset( tvdParent->pstRemoteFileServer->acLoginPass , 0x00 , sizeof(tvdParent->pstRemoteFileServer->acLoginPass) );
		return -3;
	}

	DEBUGLOGC( "rfb->str_len[%d] ->buf[%.*s] " , rfb->str_len , rfb->str_len,rfb->buf )
	DEBUGHEXLOGC( rfb->buf , (long)(rfb->str_len) , "rfb->str_len[%d] ->remain_len[%d]" , rfb->str_len , rfb->remain_len )
	p1 = rfb->buf ;
	while( (*p1) )
	{
		p2 = strchr( p1+1 , '\n' ) ;
		if( p2 )
		{
			(*p2) = '\0' ;
			if( p2-1 > p1 && *(p2-1) == '\r' )
				*(p2-1) = '\0' ;
		}
		else
		{
			p2 = p1 + strlen(p1) ;
		}
		
		chFileType = (*p1) ;

		for( int n = 0 ; n < 8 ; n++ )
		{
			/* 搜索直到非空格 */
			for( ; (*p1) ; p1++ )
				if( (*p1) == ' ' )
					break;
			if( (*p1) == '\0' )
				break;

			/* 搜索直到空格 */
			for( ; (*p1) ; p1++ )
				if( (*p1) != ' ' )
					break;
			if( (*p1) == '\0' )
				break;
		}
		if( (*p1) == ' ' )
		{
			p1 = p2 + 1 ;
			continue;
		}
		memset( acFilename , 0x00 , sizeof(acFilename) );
		strncpy( acFilename , p1 , sizeof(acFilename)-1 );

		if( strcmp(acFilename,".") == 0 || strcmp(acFilename,"..") == 0 )
		{
			p1 = p2 + 1 ;
			continue;
		}

		if( chFileType == 'd' )
		{
			memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s/" , tvdParent->acPathFilename , acFilename );
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageClosedFold , tvdParent->pstRemoteFileServer , tvdParent->acPathFilename , acPathFilename , acFilename , FALSE ) ;
			if( tvd == NULL )
			{
				free( rfb );
				return -1;
			}
		}
		else if( chFileType == '-' )
		{
			memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s" , tvdParent->acPathFilename , acFilename );
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageTextFile , tvdParent->pstRemoteFileServer , tvdParent->acPathFilename , acPathFilename , acFilename , TRUE ) ;
			if( tvd == NULL )
			{
				free( rfb );
				return -1;
			}
		}

		p1 = p2 + 1 ;
	}

	free( rfb );

	tvdParent->bIsLoadedCompleted = TRUE ;

	return 0;
}

int LoadRemoteDriverToFileTree( HWND hwnd , struct RemoteFileServer *pnodeRemoteFileServer )
{
	char			acPathFilename[ MAX_PATH ] ;
	struct TreeViewData	*tvd = NULL ;

	memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
	if( pnodeRemoteFileServer->nAccessArea == 0 )
		snprintf( acPathFilename , sizeof(acPathFilename)-1 , "sftp://%s:%d/~/" , pnodeRemoteFileServer->acNetworkAddress , pnodeRemoteFileServer->nNetworkPort );
	else
		snprintf( acPathFilename , sizeof(acPathFilename)-1 , "sftp://%s:%d/" , pnodeRemoteFileServer->acNetworkAddress , pnodeRemoteFileServer->nNetworkPort );
	tvd = AddFileTreeNode( hwnd , TVI_ROOT , nFileTreeImageDrive , pnodeRemoteFileServer , acPathFilename , acPathFilename , pnodeRemoteFileServer->acFileServerName , FALSE ) ;
	if( tvd == NULL )
		return -1;

	return 0;
}

int LoadRemoteDriversToFileTree( HWND hwnd )
{
	struct RemoteFileServer	*pnodeRemoteFileServer = NULL ;
	int			nret = 0 ;

	list_for_each_entry( pnodeRemoteFileServer , & listRemoteFileServer , struct RemoteFileServer , nodeRemoteFileServer )
	{
		LoadRemoteDriverToFileTree( hwnd , pnodeRemoteFileServer );
	}

	return 0;
}

int UpdateFileTreeNodeChildren( HWND hwnd , HTREEITEM htiParent )
{
	struct TreeViewData	*tvdParent = NULL ;
	HTREEITEM		htiChild ;
	TVITEM			tviChild ;
	BOOL			bret ;

	int			nret = 0 ;

	::SetCursor( LoadCursor(NULL,IDC_WAIT) );

	CURL			*curl = NULL ;
	char			url[ 1024 ] ;

	tvdParent = GetTreeViewDataFromHTREEITEM( htiParent ) ;
	if( tvdParent->pstRemoteFileServer )
	{
		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_WRITEFUNCTION , ReadRemoteDirectory );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
	}

	if( tvdParent->nImageIndex == nFileTreeImageDrive && tvdParent->pstRemoteFileServer && tvdParent->bIsLoadedCompleted == FALSE )
	{
		curl_easy_setopt( curl , CURLOPT_URL , tvdParent->acPathFilename );
		nret = AppendRemoteFileTreeNodeChildren( hwnd , tvdParent , curl ) ;
		if( nret )
			return nret;
	}

	htiChild = TreeView_GetChild( hwnd , htiParent ) ;
	while( htiChild )
	{
		tviChild.mask = TVIF_HANDLE ;
		tviChild.hItem = htiChild ;
		bret = TreeView_GetItem( hwnd , & tviChild ) ;
		if( bret != TRUE )
		{
			::SetCursor( LoadCursor(NULL,IDC_ARROW) );
			if( tvdParent->pstRemoteFileServer )
				curl_easy_cleanup( curl );
			return -1;
		}

		struct TreeViewData *tvdChild = (struct TreeViewData *)(tviChild.lParam) ;
		if( tvdChild->bIsLoadedCompleted == FALSE )
		{
			if( tvdChild->pstRemoteFileServer == NULL )
			{
				nret = AppendFileTreeNodeChildren( hwnd , tvdChild ) ;
			}
			else
			{
				memset( url , 0x00 , sizeof(url) );
				strncpy( url , tvdChild->acPathFilename , sizeof(url)-1 );
				curl_easy_setopt( curl , CURLOPT_URL , url );

				nret = AppendRemoteFileTreeNodeChildren( hwnd , tvdChild , curl ) ;
			}
			if( nret )
			{
				::SetCursor( LoadCursor(NULL,IDC_ARROW) );
				if( tvdChild->pstRemoteFileServer )
					curl_easy_cleanup( curl );
				return nret;
			}
		}

		htiChild = TreeView_GetNextSibling( hwnd , htiChild ) ;
	}

	if( tvdParent->pstRemoteFileServer )
		curl_easy_cleanup( curl );

	::SetCursor( LoadCursor(NULL,IDC_ARROW) );

	return 0;
}

int OnFileTreeNodeExpanding( NMTREEVIEW *lpnmtv )
{
	if( lpnmtv->action != 2 )
		return 0;

	UpdateFileTreeNodeChildren( g_hwndFileTree , lpnmtv->itemNew.hItem );

	return 0;
}

int OnFileTreeNodeDbClick()
{
	HTREEITEM		hti ;
	struct DocTypeConfig	*pstDocTypeConfig ;
	char			acExtname[ MAX_PATH ] ;

	int			nret ;

	hti = TreeView_GetSelection( g_hwndFileTree ) ;
	if( hti == NULL )
		return 0;
	struct TreeViewData *tvd = GetTreeViewDataFromHTREEITEM( hti ) ;
	if( tvd->bIsLoadedCompleted == FALSE )
		return UpdateFileTreeNodeChildren( g_hwndFileTree , hti ) ;
	if( tvd->nImageIndex != nFileTreeImageTextFile )
		return 1;
	
	SplitPathFilename( tvd->acPathFilename , NULL , NULL , NULL , NULL , NULL , acExtname );
	pstDocTypeConfig = GetDocTypeConfig( acExtname ) ;
	if( pstDocTypeConfig == NULL )
	{
		int ID = MessageBox( NULL , "该文件不在文本编辑器支持文件类型中，是否继续打开？" , "文件类型警告" , MB_ICONWARNING|MB_YESNO  ) ;
		if( ID == IDNO )
			return 0;
	}

	if( tvd->pstRemoteFileServer == NULL )
		nret = OpenFileDirectly( tvd->acPathFilename ) ;
	else
		nret = OpenRemoteFileDirectly( tvd->pstRemoteFileServer , tvd->acPathFilename ) ;
	if( nret )
		return nret;

	return 0;
}

int RefreshFileTree()
{
	HTREEITEM		htiSelect ;
	struct TreeViewData	*tvd = NULL ;
	struct TreeViewData	tvdSelect ;
	HTREEITEM		htiParent ;
	struct TreeViewData	*tvdParent = NULL ;
	HTREEITEM		htiFirstChild ;
	HTREEITEM		htiChild ;
	TVITEM			tviChild ;

	BOOL			bret ;

	int			nret ;

	htiSelect = TreeView_GetSelection( g_hwndFileTree ) ;
	if( htiSelect == NULL )
		return 0;
	tvd = GetTreeViewDataFromHTREEITEM( htiSelect ) ;
	memcpy( & tvdSelect , tvd , sizeof(struct TreeViewData) );

	htiParent = TreeView_GetParent( g_hwndFileTree , htiSelect ) ;
	if( htiParent == NULL )
		return 0;
	tvdParent = GetTreeViewDataFromHTREEITEM( htiParent ) ;
	while(1)
	{
		htiFirstChild = TreeView_GetChild( g_hwndFileTree , htiParent ) ;
		if( htiFirstChild == NULL )
			break;
		tvd = GetTreeViewDataFromHTREEITEM( htiFirstChild ) ;
		free( tvd );

		TreeView_DeleteItem( g_hwndFileTree , htiFirstChild );
	}

	if( tvdParent->pstRemoteFileServer == NULL )
	{
		nret = AppendFileTreeNodeChildren( g_hwndFileTree , tvdParent ) ;
		if( nret )
			return nret;
	}
	else
	{
		CURL			*curl = NULL ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_WRITEFUNCTION , ReadRemoteDirectory );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );

		curl_easy_setopt( curl , CURLOPT_URL , tvdParent->acPathFilename );

		nret = AppendRemoteFileTreeNodeChildren( g_hwndFileTree , tvdParent , curl ) ;
		if( nret )
		{
			curl_easy_cleanup( curl );
			return -1;
		}

		curl_easy_cleanup( curl );
	}

	nret = UpdateFileTreeNodeChildren( g_hwndFileTree , htiParent ) ;
	if( nret )
		return nret;

	htiChild = TreeView_GetChild( g_hwndFileTree , htiParent ) ;
	while( htiChild )
	{
		tviChild.mask = TVIF_HANDLE ;
		tviChild.hItem = htiChild ;
		bret = TreeView_GetItem( g_hwndFileTree , & tviChild ) ;
		if( bret != TRUE )
			return -1;

		struct TreeViewData *tvdChild = (struct TreeViewData *)(tviChild.lParam) ;
		if( strcmp(tvdChild->acPathFilename,tvdSelect.acPathFilename) == 0 && strcmp(tvdChild->acFilename,tvdSelect.acFilename) == 0 )
		{
			TreeView_SelectItem( g_hwndFileTree , htiChild );
			break;
		}

		htiChild = TreeView_GetNextSibling( g_hwndFileTree , htiChild ) ;
	}

	return 0;
}

int FileTreeRenameDirectory()
{
	HTREEITEM		htiSelect ;
	struct TreeViewData	*tvdSelect = NULL ;
	char			acOldDirectoryName[ MAX_PATH ] ;
	char			acNewDirectoryName[ MAX_PATH ] ;
	char			acNewDirectoryPathName[ MAX_PATH ] ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( g_hwndFileTree ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = GetTreeViewDataFromHTREEITEM( htiSelect ) ;

	memset( acOldDirectoryName , 0x00 , sizeof(acOldDirectoryName) );
	strncpy( acOldDirectoryName , tvdSelect->acFilename , sizeof(acOldDirectoryName)-1 );
	strcpy( acNewDirectoryName , acOldDirectoryName );
	nret = InputBox( g_hwndMainWindow , "请输入新目录名：" , "输入窗口" , 0 , acNewDirectoryName , sizeof(acNewDirectoryName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acNewDirectoryPathName , sizeof(acNewDirectoryPathName)-1 , "%s/%s" , tvdSelect->acPathName , acNewDirectoryName );
		BOOL bret = ::MoveFile( tvdSelect->acPathFilename , acNewDirectoryPathName ) ;
		if( bret == FALSE )
		{
			ErrorBox( "目录[%s]改名[%s]失败，请检查目录是否非空" , acOldDirectoryName , acNewDirectoryName );
			return -1;
		}
		else
		{
			TVITEM	tvi ;
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			tvi.mask = TVIF_HANDLE|TVIF_TEXT ;
			tvi.hItem = htiSelect ;
			tvi.pszText = acNewDirectoryName ;
			TreeView_SetItem( g_hwndFileTree , & tvi );

			strncpy( tvdSelect->acFilename , acNewDirectoryName , sizeof(tvdSelect->acFilename)-1 );
			memset( tvdSelect->acPathFilename , 0x00 , sizeof(tvdSelect->acPathFilename) );
			snprintf( tvdSelect->acPathFilename , sizeof(tvdSelect->acPathFilename)-1 , "%s/%s" , tvdSelect->acPathName , tvdSelect->acFilename );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		snprintf( url , sizeof(url)-1 , "%s" , tvdSelect->acPathName );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathName , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rename %s%s %s%s" , p+1,tvdSelect->acFilename , p+1,acNewDirectoryName );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rename /%s%s /%s%s" , p+1,tvdSelect->acFilename , p+1,acNewDirectoryName );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			ErrorBox( "目录[%s]改名[%s]失败[%d][%s]，请检查目录是否非空" , tvdSelect->acFilename , acNewDirectoryName , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			TVITEM	tvi ;
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			tvi.mask = TVIF_HANDLE|TVIF_TEXT ;
			tvi.hItem = htiSelect ;
			tvi.pszText = acNewDirectoryName ;
			TreeView_SetItem( g_hwndFileTree , & tvi );

			strncpy( tvdSelect->acFilename , acNewDirectoryName , sizeof(tvdSelect->acFilename)-1 );
			memset( tvdSelect->acPathFilename , 0x00 , sizeof(tvdSelect->acPathFilename) );
			snprintf( tvdSelect->acPathFilename , sizeof(tvdSelect->acPathFilename)-1 , "%s%s/" , tvdSelect->acPathName , tvdSelect->acFilename );
		}

		curl_easy_cleanup( curl );
	}

	::UpdateWindow( g_hwndFileTree );

	return 0;
}

int FileTreeDeleteDirectory()
{
	HTREEITEM		htiSelect ;
	struct TreeViewData	*tvdSelect = NULL ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( g_hwndFileTree ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = GetTreeViewDataFromHTREEITEM( htiSelect ) ;

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		BOOL bret = ::RemoveDirectory( tvdSelect->acPathFilename ) ;
		if( bret == FALSE )
		{
			ErrorBox( "删除目录[%s]失败，请检查目录是否非空" , tvdSelect->acPathFilename );
			return -1;
		}
		else
		{
			TreeView_DeleteItem( g_hwndFileTree , htiSelect );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_URL , tvdSelect->acPathName );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathFilename , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rmdir %.*s" , strlen(p+1)-1 , p+1 );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rmdir /%.*s" , strlen(p+1)-1 , p+1 );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			ErrorBox( "删除目录[%s]失败[%d][%s]，请检查目录是否非空" , tvdSelect->acPathFilename , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			TreeView_DeleteItem( g_hwndFileTree , htiSelect );
		}

		curl_easy_cleanup( curl );
	}

	::UpdateWindow( g_hwndFileTree );

	return 0;
}

int FileTreeCreateSubDirectory()
{
	HTREEITEM		htiSelect ;
	struct TreeViewData	*tvdSelect = NULL ;
	char			acDirectoryName[ MAX_PATH ] ;
	char			acPathDirectoryName[ MAX_PATH ] ;
	char			acPathName[ MAX_PATH ] ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( g_hwndFileTree ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = GetTreeViewDataFromHTREEITEM( htiSelect ) ;

	memset( acDirectoryName , 0x00 , sizeof(acDirectoryName) );
	nret = InputBox( g_hwndMainWindow , "请输入子目录名：" , "输入窗口" , 0 , acDirectoryName , sizeof(acDirectoryName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acPathDirectoryName , sizeof(acPathDirectoryName)-1 , "%s\\%s" , tvdSelect->acPathFilename , acDirectoryName );
		strcpy( acPathName , tvdSelect->acPathFilename );

		BOOL bret = ::CreateDirectory( acPathDirectoryName , NULL ) ;
		if( bret == FALSE )
		{
			ErrorBox( "创建目录[%s]失败" , acPathDirectoryName );
			return -1;
		}
		else
		{
			AddFileTreeNode( g_hwndFileTree , htiSelect , nFileTreeImageClosedFold , NULL , acPathName , acPathDirectoryName , acDirectoryName , FALSE );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		snprintf( acPathDirectoryName , sizeof(acPathDirectoryName)-1 , "%s%s/" , tvdSelect->acPathFilename , acDirectoryName );
		strcpy( acPathName , tvdSelect->acPathFilename );

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		snprintf( url , sizeof(url)-1 , "%s" , tvdSelect->acPathFilename );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		memset( postquote , 0x00 , sizeof(postquote) );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathFilename , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "mkdir %s%s" , p+1 , acDirectoryName );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "mkdir /%s%s" , p+1 , acDirectoryName );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			ErrorBox( "创建子目录[%s]失败[%d][%s]" , acPathDirectoryName , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			AddFileTreeNode( g_hwndFileTree , htiSelect , nFileTreeImageClosedFold , tvdSelect->pstRemoteFileServer , acPathName , acPathDirectoryName , acDirectoryName , FALSE );
		}

		curl_easy_cleanup( curl );
	}

	::UpdateWindow( g_hwndFileTree );

	return 0;
}

static size_t WriteRemoteFile(void *buffer, size_t size, size_t nmemb, void *stream)
{
	return 0;
}

int FileTreeCreateFile()
{
	HTREEITEM		htiSelect ;
	struct TreeViewData	*tvdSelect = NULL ;
	char			acFileName[ MAX_PATH ] ;
	char			acPathFilename[ MAX_PATH ] ;
	char			acPathName[ MAX_PATH ] ;
	struct TreeViewData	*tvdCreate = NULL ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( g_hwndFileTree ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = GetTreeViewDataFromHTREEITEM( htiSelect ) ;

	memset( acFileName , 0x00 , sizeof(acFileName) );
	nret = InputBox( g_hwndMainWindow , "请输入文件名：" , "输入窗口" , 0 , acFileName , sizeof(acFileName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s/%s" , tvdSelect->acPathFilename , acFileName );
		strcpy( acPathName , tvdSelect->acPathFilename );

		FILE *fp = fopen( acPathFilename , "r" ) ;
		if( fp )
		{
			fclose( fp );
			ErrorBox( "文件[%s]已存在" , acPathFilename );
			return -1;
		}

		fp = fopen( acPathFilename , "w" ) ;
		if( fp == NULL )
		{
			ErrorBox( "创建文件[%s]失败" , acPathFilename );
			return -1;
		}
		else
		{
			fclose( fp );
			tvdCreate = AddFileTreeNode( g_hwndFileTree , htiSelect , nFileTreeImageTextFile , NULL , acPathName , acPathFilename , acFileName , TRUE ) ;
			TreeView_SelectItem( g_hwndFileTree , tvdCreate->hti );
			OnFileTreeNodeDbClick();
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			userpwd[ 256 ] ;

		snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s" , tvdSelect->acPathFilename , acFileName );
		strcpy( acPathName , tvdSelect->acPathFilename );

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		snprintf( url , sizeof(url)-1 , "%s%s" , tvdSelect->acPathFilename , acFileName );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_READFUNCTION , WriteRemoteFile );
		curl_easy_setopt( curl , CURLOPT_UPLOAD , 1L );
		curl_easy_setopt( curl , CURLOPT_READDATA , NULL );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			ErrorBox( "创建文件[%s]失败[%d][%s]" , acPathFilename , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			tvdCreate = AddFileTreeNode( g_hwndFileTree , htiSelect , nFileTreeImageTextFile , tvdSelect->pstRemoteFileServer , acPathName , acPathFilename , acFileName , TRUE ) ;
			TreeView_SelectItem( g_hwndFileTree , tvdCreate->hti );
			OnFileTreeNodeDbClick();
		}

		curl_easy_cleanup( curl );
	}

	::UpdateWindow( g_hwndFileTree );

	return 0;
}

int FileTreeCopyFile()
{
	HTREEITEM		htiSelect ;
	struct TreeViewData	*tvdSelect = NULL ;
	char			acNewFileName[ MAX_PATH ] ;
	char			acNewPathFilename[ MAX_PATH ] ;
	struct TreeViewData	*tvdCreate = NULL ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( g_hwndFileTree ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = GetTreeViewDataFromHTREEITEM( htiSelect ) ;

	strcpy( acNewFileName , tvdSelect->acFilename );
	nret = InputBox( g_hwndMainWindow , "请输入文件名：" , "输入窗口" , 0 , acNewFileName , sizeof(acNewFileName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acNewPathFilename , sizeof(acNewPathFilename)-1 , "%s/%s" , tvdSelect->acPathName , acNewFileName );

		BOOL bret = ::CopyFile( tvdSelect->acPathFilename , acNewPathFilename , TRUE ) ;
		if( bret == FALSE )
		{
			ErrorBox( "复制文件[%s]失败" , acNewPathFilename );
			return -1;
		}
		else
		{
			tvdCreate = AddFileTreeNode( g_hwndFileTree , TreeView_GetParent(g_hwndFileTree,htiSelect) , nFileTreeImageTextFile , NULL , tvdSelect->acPathName , acNewPathFilename , acNewFileName , TRUE ) ;
			TreeView_SelectItem( g_hwndFileTree , tvdCreate->hti );
		}
	}
	else
	{
#if 0
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		snprintf( acNewPathFilename , sizeof(acNewPathFilename)-1 , "%s%s" , tvdSelect->acPathName , acNewFileName );
		// strcpy( acPathName , tvdSelect->acPathFilename );


		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		strncpy( url , pnodeTabPage->acPathFilename , sizeof(url)-1 );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , pnodeTabPage->stRemoteFileServer.acLoginUser , pnodeTabPage->stRemoteFileServer.acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_UPLOAD , 1L );
		curl_easy_setopt( curl , CURLOPT_READFUNCTION , WriteRemoteFile );
		curl_easy_setopt( curl , CURLOPT_READDATA , pnodeTabPage );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			::MessageBox(NULL, TEXT("连接远程文件服务器失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
			curl_easy_cleanup( curl );
			return -3;
		}
		else
		{
			curl_easy_cleanup( curl );
		}
#endif
		ErrorBox( "暂时不支持远程SFTP复制文件" );
	}

	::UpdateWindow( g_hwndFileTree );

	return 0;
}

int FileTreeRenameFile()
{
	HTREEITEM		htiSelect ;
	struct TreeViewData	*tvdSelect = NULL ;
	char			acOldFileName[ MAX_PATH ] ;
	char			acNewFileName[ MAX_PATH ] ;
	char			acNewPathFileName[ MAX_PATH ] ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( g_hwndFileTree ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = GetTreeViewDataFromHTREEITEM( htiSelect ) ;

	memset( acOldFileName , 0x00 , sizeof(acOldFileName) );
	strncpy( acOldFileName , tvdSelect->acFilename , sizeof(acOldFileName)-1 );
	strcpy( acNewFileName , acOldFileName );
	nret = InputBox( g_hwndMainWindow , "请输入新文件名：" , "输入窗口" , 0 , acNewFileName , sizeof(acNewFileName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acNewPathFileName , sizeof(acNewPathFileName)-1 , "%s/%s" , tvdSelect->acPathName , acNewFileName );
		BOOL bret = ::MoveFile( tvdSelect->acPathFilename , acNewPathFileName ) ;
		if( bret == FALSE )
		{
			ErrorBox( "目录[%s]改名[%s]失败，请检查目录是否非空" , acOldFileName , acNewFileName );
			return -1;
		}
		else
		{
			TVITEM	tvi ;
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			tvi.mask = TVIF_HANDLE|TVIF_TEXT ;
			tvi.hItem = htiSelect ;
			tvi.pszText = acNewFileName ;
			TreeView_SetItem( g_hwndFileTree , & tvi );

			strncpy( tvdSelect->acFilename , acNewFileName , sizeof(tvdSelect->acFilename)-1 );
			memset( tvdSelect->acPathFilename , 0x00 , sizeof(tvdSelect->acPathFilename) );
			snprintf( tvdSelect->acPathFilename , sizeof(tvdSelect->acPathFilename)-1 , "%s/%s" , tvdSelect->acPathName , tvdSelect->acFilename );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		snprintf( url , sizeof(url)-1 , "%s" , tvdSelect->acPathName );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathName , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rename %s%s %s%s" , p+1,tvdSelect->acFilename , p+1,acNewFileName );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rename /%s%s /%s%s" , p+1,tvdSelect->acFilename , p+1,acNewFileName );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			ErrorBox( "文件[%s]改名[%s]失败[%d][%s]" , tvdSelect->acFilename , acNewFileName , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			TVITEM	tvi ;
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			tvi.mask = TVIF_HANDLE|TVIF_TEXT ;
			tvi.hItem = htiSelect ;
			tvi.pszText = acNewFileName ;
			TreeView_SetItem( g_hwndFileTree , & tvi );

			strncpy( tvdSelect->acFilename , acNewFileName , sizeof(tvdSelect->acFilename)-1 );
			memset( tvdSelect->acPathFilename , 0x00 , sizeof(tvdSelect->acPathFilename) );
			snprintf( tvdSelect->acPathFilename , sizeof(tvdSelect->acPathFilename)-1 , "%s%s" , tvdSelect->acPathName , tvdSelect->acFilename );
		}

		curl_easy_cleanup( curl );
	}

	::UpdateWindow( g_hwndFileTree );

	return 0;
}

int FileTreeDeleteFile()
{
	HTREEITEM		htiSelect ;
	struct TreeViewData	*tvdSelect = NULL ;
	int			nTabPagesCount , nTabPageIndex ;
	struct TabPage		*pnodeTabPage = NULL ;
	TCITEM			tci ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( g_hwndFileTree ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = GetTreeViewDataFromHTREEITEM( htiSelect ) ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		pnodeTabPage = (struct TabPage *)(tci.lParam);
		if( strcmp( pnodeTabPage->acPathFilename , tvdSelect->acPathFilename ) == 0 )
		{
			nret = OnCloseFile( pnodeTabPage ) ;
			if( nret )
				return NULL;
			break;
		}
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		BOOL bret = ::DeleteFile( tvdSelect->acPathFilename ) ;
		if( bret == FALSE )
		{
			ErrorBox( "删除文件[%s]失败" , tvdSelect->acPathFilename );
			return -1;
		}
		else
		{
			TreeView_DeleteItem( g_hwndFileTree , htiSelect );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_URL , tvdSelect->acPathName );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathFilename , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rm %.*s" , strlen(p+1) , p+1 );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rm /%.*s" , strlen(p+1) , p+1 );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			ErrorBox( "删除文件[%s]失败[%d][%s]" , tvdSelect->acPathFilename , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			TreeView_DeleteItem( g_hwndFileTree , htiSelect );
		}

		curl_easy_cleanup( curl );
	}

	::UpdateWindow( g_hwndFileTree );

	return 0;
}

int LocateDirectory( char *acPathName )
{
	char			*acPathNameDup = NULL ;
	char			*pcDirectoryName = NULL ;
	HTREEITEM		hti = NULL , htiChild = NULL ;
	struct TreeViewData	*tvd = NULL ;
	int			nret = 0 ;
	BOOL			bret = TRUE ;

	acPathNameDup = _strdup(acPathName) ;
	if( acPathNameDup == NULL )
		return -1;

	htiChild = TreeView_GetRoot( g_hwndFileTree ) ;
	pcDirectoryName = strtok( acPathNameDup , "\\/" ) ;
	while( pcDirectoryName )
	{
		hti = htiChild ;

		do
		{
			tvd = GetTreeViewDataFromHTREEITEM( hti ) ;
			if( tvd == NULL )
			{
				break;
			}
			else if( tvd->bIsLoadedCompleted == FALSE )
			{
				nret = AppendFileTreeNodeChildren( g_hwndFileTree , tvd ) ;
				if( nret )
				{
					free( acPathNameDup );
					return nret;
				}
			}
			if( strcmp(tvd->acFilename,pcDirectoryName) == 0 )
				break;

			hti = TreeView_GetNextSibling( g_hwndFileTree , hti ) ;
		}
		while( hti );
		if( hti == NULL )
			break;

		htiChild = TreeView_GetChild( g_hwndFileTree , hti ) ;
		pcDirectoryName = strtok( NULL , "\\/" ) ;
	}
	if( pcDirectoryName == NULL )
	{
		g_bIsFileTreeBarShow = TRUE ;
		TreeView_SelectItem( g_hwndFileTree , hti );
		::SendMessage( g_hwndFileTree , WM_SETFOCUS , 0 , 0 );
		UpdateAllWindows( g_hwndMainWindow );
	}

	free( acPathNameDup );

	return 0;
}
